# Gitlab CI/CD 설정

## Git flow 및 관련 기술정보 링크
우아한형제들 기술블로그 : https://techblog.woowahan.com/2553/    
코딩애플 : https://www.youtube.com/watch?v=EV3FZ3cWBp8  
화해 기술블로그 : https://blog.hwahae.co.kr/all/tech/9507  

여러 IT Blog :
https://puleugo.tistory.com/107  
https://medium.com/corca/%EC%8B%A4%EB%AC%B4%EC%97%90%EC%84%9C-%EC%82%AC%EC%9A%A9%EB%90%98%EB%8A%94-git-flow-%EC%82%AC%EC%9A%A9%EB%B2%95-aka-app%EA%B0%9C%EB%B0%9C%EC%97%90-%EC%93%B0%EC%9D%B4%EB%8A%94-git-flow-%EB%B8%8C%EB%9E%9C%EC%B9%AD-%EC%A0%84%EB%9E%B5-9e860d7ce771  

gitlab 관련 정보 :   
https://workshop.infograb.io/gitlab-ci/11_introduction-to-gitlab-cicd/4_quick_start_gitlab_ci_cd/  
https://docs.gitlab.com/ee/user/project/releases/release_cicd_examples.html   
https://www.youtube.com/watch?v=bW7VMeSHFNg  (nana gitlab ci/cd python project and 강좌링크-0-)  
https://www.youtube.com/watch?v=qP8kir2GUgo&t=906s  (gitlab ci/cd springboot backend maven project)  


https://www.youtube.com/results?search_query=gitlab+ci+cd+pipline+for+java+application+gradle    
https://www.youtube.com/watch?v=PUvAHlryrSU (gradle Best Practices in Gitlab)
https://www.youtube.com/watch?v=oELKRUiXW9Y (gradle Aws) 

## 파이프라인 구성을 위한 요소 파악  
- Dockerfile 이용해서 실행가능한 docker 파일생성
- docker-compose.yml 파일 생성
- gitlab 연동을 위한 ".gitlab-ci.yml" 파일 생성
- Jenkinsfile 파일 생성


### Dockerfile 생성
Root 프로젝트 위치
Docker-compose.yml 을 이용해서 K8S에 구성될수 있는 정보들을 생성  

실행 cmd : 
> docker build -t myapp .
 
```shell
# 빌드용 이미지
# OS 위에 사용하고자하는 JDK 버전을 빌드
FROM amazoncorretto:17 as build

# 작업 디렉토리 설정
# 디렉토리를 설정 함.
WORKDIR /build

# gradle 빌드 캐시를 효율적으로 사용하기 위해서 레이어를 분리
COPY gradlew .
COPY gradle gradle
COPY build.gradle.kts .
COPY settings.gradle.kts .

# 추가 설정 파일이나 디렉토리를 복사
COPY src src

# 빌드 이미지 애플리케이션 빌드
# RUN ./gradlew build -x test 이 상태가 default 이걸 돌리면 겁나 오래걸린다 
# ..이부분은 좀더 정보가 필요한 사항 
RUN gradle clean build -x test --parallel --continue > /dev/null 2>&1 || true

## 빌드 결과물을 실행한 이미지 준비
FROM amazoncorretto:17
WORKDIR /app

# 빌드 단계에서 생성된 JAR 파일을 복사 API멀티모듈정보를 app.jar로 Docker 이미지로 
COPY --from=build /build/api/build/libs/api.jar app.jar

# 포트 열기
# 포트 정보는 아래 기본값으로 실행
EXPOSE 8888

# 애플리케이션 실행
CMD ["java", "-jar", "app.jar"]

```

### docker-compose.yml
springboot web api 를 실행하기위한 docker-compose.yml 생성  
docker-compose.yml 에 dockerfile 또는 image로 생성해서 실행 가능   
volumes는 권한이 필요함. 

실행 cmd : 
> docker-compose up -d
```shell
version: '3.8'
services:
  app:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "8888:8888"
    environment:
      SPRING_PROFILES_ACTIVE: docker
    volumes:
      - ./data:/data

```

### .gitlab-ci.yml 설정 정보 
gitlab project > Build 구성
Piplines, Jobs, Pipeline editor, pipeline schedules
![스크린샷 2024-02-10 오후 7 43 06](https://github.com/lswteen/boilerplate/assets/3292892/fa6b7108-ce64-4dcb-aefa-0db12ec16d9a)


gitlab project > Build > Pipelines 클릭  
.gitlab-ciyml 에 정의한대로   
build a,b  
test a,b   
실행 에 대한 정보 확인 가능
![스크린샷 2024-02-10 오후 7 44 53](https://github.com/lswteen/boilerplate/assets/3292892/948bcc07-bd71-4d19-9364-f89779b64029)



```shell
stages:
  - build
  - test

image: alpine:latest

variables:
  GREETING_MESSAGE: Hello

build_a:
  stage: build
  script:
    - echo "$GREETING_MESSAGE, GitLab CI!"
    - echo "이 Job은 무언가를 빌드합니다."

build_b:
  stage: build
  script:
    - echo "이 Job은 다른 무언가를 빌드합니다."
    - sleep 5

test_a:
  stage: test
  script:
    - echo "이 Job은 무언가를 테스트합니다."
    - echo "빌드 단계의 모든 Job이 완료된 경우에만 실행됩니다."

test_b:
  stage: test
  script:
    - echo "이 Job은 다른 무언가를 테스트합니다."
    - echo "이 Job도 빌드 단계의 모든 Job이 완료된 경우에만 실행됩니다."
    - echo "test_a와 거의 동시에 시작됩니다."
    - sleep 5
```

### gitlab + jenkins gradle project에 필요한 파일 설정

이부분은 진행중상태...

```shell
pipeline {
    agent any // 모든 에이전트에서 실행 가능

    environment {
        // 환경 변수 설정
        GRADLE_HOME = '/opt/gradle/latest'
        PATH = "$GRADLE_HOME/bin:$PATH"
    }

    stages {
        stage('Checkout') {
            steps {
                // GitLab에서 최신 코드 체크아웃
                checkout scm
            }
        }
        stage('Build') {
            steps {
                // Gradle을 사용한 프로젝트 빌드
                sh './gradlew build'
            }
        }
        stage('Test') {
            steps {
                // Gradle을 사용한 테스트 실행
                sh './gradlew test'
            }
        }
        stage('Deploy') {
            steps {
                // 배포 단계 (예: 스크립트 실행 또는 도구 사용)
                // sh './deploy.sh'
            }
        }
    }
}

```


### 배포 요청시 정책 Merge Request 사용시 gitlab 라벨
Project > Labels > 프로젝트에 필요한 라벨 생성

- code review
- dev -1
- dev -2
- dev -3
- dev -4
- dev merge
- stg merge
- release merge

![스크린샷 2024-02-10 오후 7 52 38](https://github.com/lswteen/boilerplate/assets/3292892/65bf6bc1-cf83-4355-bdd2-7e31cd5d278e)
