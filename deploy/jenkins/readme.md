# Jenkins 설명
docker-compose.yml 생성
```yml
version: '3.8'
services:
  jenkins:
    image: jenkins/jenkins:lts
    container_name: jenkins
    ports:
      - "8080:8080"
      - "50000:50000"
    volumes:
      - jenkins_home:/var/jenkins_home
    environment:
      - JENKINS_OPTS=--prefix=/jenkins
volumes:
  jenkins_home:

```
## 백그라운드 실행
실행전 8080, 50000포트 사용하고있는지 확인 필요
> docker-compose up -d 

## 디폴트 admin 패스워드 확인
아래 명령어로 터미널에 뜨는 패스워드 확인
> jenkins % docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```shell
(base) lswteen@lswteenui-iMac jenkins % docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword

2a6e1ee9a8124a80aa321168cb033952
```

## jenkins web ui 접속 
http://localhost:8080/jenkins/
아래 그림 나오기전 터미널에서 확인한 패스워드 입력시 아래 화면으로 이동
![스크린샷 2024-02-10 오후 6 05 20](https://github.com/lswteen/boilerplate/assets/3292892/7bf2ee49-8282-40ad-bd00-44009fec0632)


## Install suggested plugins
권장 사항: Jenkins를 처음 사용하는 경우이거나 일반적인 기능을 사용할 계획이라면 "Install suggested plugins" 옵션을 선택하는 것이 좋습니다. 이 옵션은 Jenkins 개발자들이 가장 일반적으로 사용되고 유용하다고 판단하는 플러그인 세트를 자동으로 설치합니다. 이 세트에는 버전 관리(예: Git), 빌드 도구(예: Maven, Gradle), 지속적인 통합(CI), 지속적인 배포(CD)를 지원하는 플러그인들이 포함됩니다. 대부분의 사용자에게 필요한 기본적인 기능을 제공하기 때문에 대부분의 경우 이 옵션을 선택하는 것이 좋습니다.

## Select plugins to install
고급 사용자용: 특정한 플러그인만 설치하고 싶거나 Jenkins 설치를 더 세밀하게 제어하고 싶은 경우 "Select plugins to install" 옵션을 선택합니다. 이 옵션을 선택하면 설치할 플러그인을 개별적으로 선택할 수 있는 화면이 나타납니다. 특정 작업을 위해 필요한 플러그인이 있거나, 기본 제공되는 플러그인 이외에 추가적인 기능이 필요한 경우 이 옵션을 사용하면 됩니다. 하지만, 어떤 플러그인이 필요한지 잘 모르겠다면 이 옵션은 초보자에게 다소 어려울 수 있습니다.

## Install suggested plugins 선택 후 설치 
<img width="981" alt="스크린샷 2024-02-10 오후 7 07 27" src="https://github.com/lswteen/boilerplate/assets/3292892/873d37a4-f7ab-4e7d-b583-a85caf279d8f">


## 계정 정보 초기 입력
id : renzo1980  
pw : 늘쓰던 패스워드  
mail : lswteen@gmail.com

## jenkins 홈페이지 화면 이동
<img width="1428" alt="스크린샷 2024-02-10 오후 7 10 13" src="https://github.com/lswteen/boilerplate/assets/3292892/44457cd8-16d4-4cfa-965f-a37310e4795a">
