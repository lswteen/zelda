package com.renzo.zelda.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @GetMapping("/v1/samples")
    public String samples(){
        return "hello world";
    }

    @PostMapping("/v1/samples")
    public String samples(String requet){
        return "hello world";
    }
}
